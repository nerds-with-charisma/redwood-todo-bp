# Setting up a new Redwood Project
• You must have node v 12. For some reason my machine never remembers this..
`export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm`

`nvm use 12`

• Create a new project
`yarn create redwood-app ./<name>`
ex: `yarn create redwood-app ./todo`

`cd <name>`
`yarn rw dev`

• Opens on:
web:      localhost:8910
graphql:  localhost:8911/graphql

# Installing NWC deps
• Install
`cd web`
`npm i @nerds-with-charisma/nerds-style-sass`
`cd ..`

• Import either scss or css in your stylesheet in /web/src/index.css
`@import "~@nerds-with-charisma/nerds-style-sass/main.scss";`
- or -
`@import "~@nerds-with-charisma/nerds-style-sass/main.css";`

# Initialize Git
`git init`
`git add .`
`git commit -m "Init commit"`

NOTE: to check git history run `git log --pretty=format:"%h - %an, %ar : %s"`

# Hook To Git Repo
`git remote add origin <url>/<project>.git`
ex: `git remote add origin git@gitlab.com:nerds-with-charisma/redwood-todo-bp.git`

`git add .`
`git commit -m "Initial commit"`
`git push -u origin master`

[https://gitlab.com/nerds-with-charisma/redwood-todo-bp](https://gitlab.com/nerds-with-charisma/redwood-todo-bp)

# Create Pages
• Create our first page
`yarn rw g page <name> <path>`
ex: `yarn rw g page home /`

** Generates files in /web/src/pages/<name>Page/<name>Page.js (as well as test and storybook files)

Creates a route for the new page in /web/src/Routes.js

## Git
c88f610 - Brian Dausman, 4 days ago : Init commit

# Create Layout
• Create the main layout
`yarn rw g layout <name>`
ex: `yarn rw g layout main`

** Generate files in /web/src/layuts/<name>Layout/<name>Layout.js (& test/sb files)

• Create minimal layout (see finished git file)

• Add layout to other pages (see finishd git file for HomePage.js)
### Git
49e663a - Brian Dausman, 30 seconds ago : Add layout

# Create Context API
• Make folders and files
`mkdir web/src/utils`
`mkdir web/src/utils/context`
`> web/src/utils/context/context.js` (See context.js git file)
`> web/src/utils/context/initialData.js` (see initialData.js git file)

• Wrap our layout in the context provider and pass it initial data in index.js (see git)

• Use it in the files we need it, ex: HomePage.js

Note: I like to use context api for ALL static stuff. That way i only have to look in one spot to update things.

# Create Users Model
• Make User model in api/prisma/schema.prisma (// users model)

• Save the db
`yarn rw db save`

• Apply changes
`yarn rw db up`

• Scaffold the CRUD operations for User
`yarn rw g scaffold user`
Note: This will create crud operations on the front end as well @ /users

• You can now perform CRUD on users @ [GQL Playground](http://localhost:8911/graphql)
Note: Obviously we don't have any users yet

+ Query: Get all users
`query {
  users {
    id
    name
    email
  }
}`

+ Mutation: Make a new User
`mutation {
  createUser(input:{
   email: "briandausman@gmail.com"
    name: "Brian Dausman"
    password: "password"
  }
) {
    id
    email
  }
}`

+ Mutation: Update a user
`mutation{
  updateUser(id: 2, input:{
    name: "Changed"
  } ){
    id
    email
    name
  }
}`

+ Mutation: Delete user
`mutation {
  deleteUser(id: 2){
    id
  }
}`

### Git
ed7de91 - Brian Dausman, 15 seconds ago : Fix user model

# Create Post model
• Make post model in api/prisma/schema.prisma (// post model)

• Scaffold
`yarn rw db save`
`yarn rw db up`
`yarn rw g scaffold post`

• Add Post to User model
`yarn rw destroy scaffold post`
`yarn rw g scaffold post`

• Make the author id service change to connect the 2
[See how](https://github.com/redwoodjs/redwood/issues/705)

+ Mutation: Create Post
`mutation {
  createPost(input:{
   body: "Test"
   title: "Some sweet title"
   authorId: 1
  }
) {
    id
  }
}`

+ Query: Get post with user info
`query {
  posts {
    id
    title
    author {
      id
      email
    }
  }
}`

+ Query: Get posts by a user
`query {
  users {
    email
    posts {
      id
      title
    }
  }
}`

+ Query: Get post
`query {
  post(id: 1) {
    title
  }
}`

### GIT
0d7ee2d - Brian Dausman, 24 seconds ago : Add post model

# Display posts on homepage
• Generate a new cell - Creates files in /web/src/components/BlogPostsCell/BlogPostsCell.js

`yarn rw g cell BlogPosts`

• In /web/src/components/BlogPostsCell/BlogPostsCell.js, replace 'blogPosts' with 'posts'

• Import and use our BlogPostsCell component in HomePage.js

• Pull in the required data inside of BlogPostsCell's query at the top

• Pretty up the BlogPostsCell component to not just be a json dump

### Git
0b884f8 - Brian Dausman, 4 seconds ago : Add posts to homepage


# Authenticate a user
• You can change the name of the routes or make the signup / login pages look nicer


### TODOS
NWC checklist (jamstack jedi)