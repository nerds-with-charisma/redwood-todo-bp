import LikesLayout from 'src/layouts/LikesLayout'
import NewLike from 'src/components/NewLike'

const NewLikePage = () => {
  return (
    <LikesLayout>
      <NewLike />
    </LikesLayout>
  )
}

export default NewLikePage
