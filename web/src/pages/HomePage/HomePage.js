import { useContext } from 'react';

import Context from '../../utils/context/context';
import MainLayout from '../../layouts/MainLayout';

import BlogPostsCell from 'src/components/BlogPostsCell';

const HomePage = () => {
  const ctx = useContext(Context);

  return (
    <MainLayout>
      {ctx.statics.pages.home.welcomeText}
      <BlogPostsCell />
    </MainLayout>
  )
}

export default HomePage
