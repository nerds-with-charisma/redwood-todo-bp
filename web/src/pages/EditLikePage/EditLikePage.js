import LikesLayout from 'src/layouts/LikesLayout'
import EditLikeCell from 'src/components/EditLikeCell'

const EditLikePage = ({ id }) => {
  return (
    <LikesLayout>
      <EditLikeCell id={id} />
    </LikesLayout>
  )
}

export default EditLikePage
