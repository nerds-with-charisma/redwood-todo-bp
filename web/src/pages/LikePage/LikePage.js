import LikesLayout from 'src/layouts/LikesLayout'
import LikeCell from 'src/components/LikeCell'

const LikePage = ({ id }) => {
  return (
    <LikesLayout>
      <LikeCell id={id} />
    </LikesLayout>
  )
}

export default LikePage
