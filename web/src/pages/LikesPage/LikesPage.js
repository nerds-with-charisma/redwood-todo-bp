import LikesLayout from 'src/layouts/LikesLayout'
import LikesCell from 'src/components/LikesCell'

const LikesPage = () => {
  return (
    <LikesLayout>
      <LikesCell />
    </LikesLayout>
  )
}

export default LikesPage
