
const MainLayout = ({ children }) => (
  <main id="layout--main">{children}</main>
)

export default MainLayout
