const initialData = {
  statics: {
    pages: {
      home: {
        welcomeText: 'Hi Everybody!',
      },
    },
  },
};

export default initialData;