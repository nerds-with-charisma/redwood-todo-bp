import { useMutation, useFlash } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import LikeForm from 'src/components/LikeForm'

export const QUERY = gql`
  query FIND_LIKE_BY_ID($id: Int!) {
    like: like(id: $id) {
      id
      createdAt
      email
      postId
    }
  }
`
const UPDATE_LIKE_MUTATION = gql`
  mutation UpdateLikeMutation($id: Int!, $input: UpdateLikeInput!) {
    updateLike(id: $id, input: $input) {
      id
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Success = ({ like }) => {
  const { addMessage } = useFlash()
  const [updateLike, { loading, error }] = useMutation(UPDATE_LIKE_MUTATION, {
    onCompleted: () => {
      navigate(routes.likes())
      addMessage('Like updated.', { classes: 'rw-flash-success' })
    },
  })

  const onSave = (input, id) => {
    const castInput = Object.assign(input, { postId: parseInt(input.postId) })
    updateLike({ variables: { id, input: castInput } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">Edit Like {like.id}</h2>
      </header>
      <div className="rw-segment-main">
        <LikeForm like={like} onSave={onSave} error={error} loading={loading} />
      </div>
    </div>
  )
}
