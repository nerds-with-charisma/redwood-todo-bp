import { useMutation, useFlash } from '@redwoodjs/web'
import { navigate, routes } from '@redwoodjs/router'
import LikeForm from 'src/components/LikeForm'

const CREATE_LIKE_MUTATION = gql`
  mutation CreateLikeMutation($input: CreateLikeInput!) {
    createLike(input: $input) {
      id
    }
  }
`

const NewLike = () => {
  const { addMessage } = useFlash()
  const [createLike, { loading, error }] = useMutation(CREATE_LIKE_MUTATION, {
    onCompleted: () => {
      navigate(routes.likes())
      addMessage('Like created.', { classes: 'rw-flash-success' })
    },
  })

  const onSave = (input) => {
    const castInput = Object.assign(input, { postId: parseInt(input.postId) })
    createLike({ variables: { input: castInput } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New Like</h2>
      </header>
      <div className="rw-segment-main">
        <LikeForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewLike
