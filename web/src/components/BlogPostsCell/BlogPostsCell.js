export const QUERY = gql`
  query BlogPostsQuery {
    posts {
      id
      body
      createdAt
      title
      author {
        id
        email
        name
      }
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Empty</div>

export const Failure = ({ error }) => <div>Error: {error.message}</div>

export const Success = ({ posts }) => (
  <section id="blogPostsCell">
    {posts.map(post => (
      <article key={post.id}>
        <header>
          <h2>{post.title}</h2>
        </header>
        <p>
          {post.body}
        </p>
        <div>
          {`Posted on ${post.createdAt} by ${post.author.name}`}
        </div>
        <br />
      </article>
    ))}
  </section>
)
