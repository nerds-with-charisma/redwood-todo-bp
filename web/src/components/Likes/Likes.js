import { useMutation, useFlash } from '@redwoodjs/web'
import { Link, routes } from '@redwoodjs/router'

const DELETE_LIKE_MUTATION = gql`
  mutation DeleteLikeMutation($id: Int!) {
    deleteLike(id: $id) {
      id
    }
  }
`

const MAX_STRING_LENGTH = 150

const truncate = (text) => {
  let output = text
  if (text && text.length > MAX_STRING_LENGTH) {
    output = output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const LikesList = ({ likes }) => {
  const { addMessage } = useFlash()
  const [deleteLike] = useMutation(DELETE_LIKE_MUTATION, {
    onCompleted: () => {
      addMessage('Like deleted.', { classes: 'rw-flash-success' })
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete like ' + id + '?')) {
      deleteLike({ variables: { id }, refetchQueries: ['LIKES'] })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Created at</th>
            <th>Email</th>
            <th>Post id</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {likes.map((like) => (
            <tr key={like.id}>
              <td>{truncate(like.id)}</td>
              <td>{timeTag(like.createdAt)}</td>
              <td>{truncate(like.email)}</td>
              <td>{truncate(like.postId)}</td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.like({ id: like.id })}
                    title={'Show like ' + like.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  <Link
                    to={routes.editLike({ id: like.id })}
                    title={'Edit like ' + like.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <a
                    href="#"
                    title={'Delete like ' + like.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(like.id)}
                  >
                    Delete
                  </a>
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default LikesList
