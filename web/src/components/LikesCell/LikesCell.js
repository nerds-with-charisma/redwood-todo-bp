import { Link, routes } from '@redwoodjs/router'

import Likes from 'src/components/Likes'

export const QUERY = gql`
  query LIKES {
    likes {
      id
      createdAt
      email
      postId
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No likes yet. '}
      <Link to={routes.newLike()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Success = ({ likes }) => {
  return <Likes likes={likes} />
}
