import { useMutation, useFlash } from '@redwoodjs/web'
import { Link, routes, navigate } from '@redwoodjs/router'

const DELETE_LIKE_MUTATION = gql`
  mutation DeleteLikeMutation($id: Int!) {
    deleteLike(id: $id) {
      id
    }
  }
`

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const Like = ({ like }) => {
  const { addMessage } = useFlash()
  const [deleteLike] = useMutation(DELETE_LIKE_MUTATION, {
    onCompleted: () => {
      navigate(routes.likes())
      addMessage('Like deleted.', { classes: 'rw-flash-success' })
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete like ' + id + '?')) {
      deleteLike({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Like {like.id} Detail
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>
              <td>{like.id}</td>
            </tr>
            <tr>
              <th>Created at</th>
              <td>{timeTag(like.createdAt)}</td>
            </tr>
            <tr>
              <th>Email</th>
              <td>{like.email}</td>
            </tr>
            <tr>
              <th>Post id</th>
              <td>{like.postId}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.editLike({ id: like.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <a
          href="#"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(like.id)}
        >
          Delete
        </a>
      </nav>
    </>
  )
}

export default Like
