import Like from 'src/components/Like'

export const QUERY = gql`
  query FIND_LIKE_BY_ID($id: Int!) {
    like: like(id: $id) {
      id
      createdAt
      email
      postId
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Like not found</div>

export const Success = ({ like }) => {
  return <Like like={like} />
}
