// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Router, Route } from '@redwoodjs/router'

const Routes = () => {
  return (
    <Router>
      <Route path="/users/new" page={NewUserPage} name="newUser" />
      <Route path="/" page={HomePage} name="home" />


      <Route path="/admin/posts/new" page={NewPostPage} name="newPost" />
      <Route path="/admin/posts/{id:Int}/edit" page={EditPostPage} name="editPost" />
      <Route path="/admin/posts/{id:Int}" page={PostPage} name="post" />
      <Route path="/admin/posts" page={PostsPage} name="posts" />
      <Route path="/admin/users/{id:Int}/edit" page={EditUserPage} name="editUser" />
      <Route path="/admin/likes/new" page={NewLikePage} name="newLike" />
      <Route path="/admin/likes/{id:Int}/edit" page={EditLikePage} name="editLike" />
      <Route path="/admin/likes/{id:Int}" page={LikePage} name="like" />
      <Route path="/admin/likes" page={LikesPage} name="likes" />
      <Route path="/admin/users/{id:Int}" page={UserPage} name="user" />
      <Route path="/admin/users" page={UsersPage} name="users" />
      <Route path="/admin/comments/new" page={NewCommentPage} name="newComment" />
      <Route path="/admin/comments/{id:Int}/edit" page={EditCommentPage} name="editComment" />
      <Route path="/admin/comments/{id:Int}" page={CommentPage} name="comment" />
      <Route path="/admin/comments" page={CommentsPage} name="comments" />




      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
