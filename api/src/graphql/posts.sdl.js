import gql from 'graphql-tag'

export const schema = gql`
  type Post {
    id: Int!
    body: String!
    createdAt: DateTime!
    title: String!
    author: User
    authorId: Int
  }

  type Query {
    posts: [Post!]!
    post(id: Int!): Post!
  }

  input CreatePostInput {
    body: String!
    title: String!
    authorId: Int
  }

  input UpdatePostInput {
    body: String
    title: String
    authorId: Int
  }

  type Mutation {
    createPost(input: CreatePostInput!): Post!
    updatePost(id: Int!, input: UpdatePostInput!): Post!
    deletePost(id: Int!): Post!
  }
`
