import gql from 'graphql-tag'

export const schema = gql`
  type Comment {
    id: Int!
    body: String!
    createdAt: DateTime!
    email: String!
    Post: Post
    postId: Int
  }

  type Query {
    comments: [Comment!]!
    comment(id: Int!): Comment!
  }

  input CreateCommentInput {
    body: String!
    email: String!
    postId: Int
  }

  input UpdateCommentInput {
    body: String
    email: String
    postId: Int
  }

  type Mutation {
    createComment(input: CreateCommentInput!): Comment!
    updateComment(id: Int!, input: UpdateCommentInput!): Comment!
    deleteComment(id: Int!): Comment!
  }
`
