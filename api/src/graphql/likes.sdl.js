import gql from 'graphql-tag'

export const schema = gql`
  type Like {
    id: Int!
    createdAt: DateTime!
    email: String!
    Post: Post
    postId: Int
  }

  type Query {
    likes: [Like!]!
    like(id: Int!): Like!
  }

  input CreateLikeInput {
    email: String!
    postId: Int
  }

  input UpdateLikeInput {
    email: String
    postId: Int
  }

  type Mutation {
    createLike(input: CreateLikeInput!): Like!
    updateLike(id: Int!, input: UpdateLikeInput!): Like!
    deleteLike(id: Int!): Like!
  }
`
